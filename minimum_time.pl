#!/usr/bin/perl

use strict;
use warnings;

# Complete the minTime function below.
sub minTime {
    my $machines = shift;
    my $goal = shift;
    my $n = shift;
    my $min_days = 1;
    my $max_days = 10**18;
    my $result;
    while ($min_days <= $max_days) {
        my $mid = sprintf("%0d", (($min_days + $max_days)/2));
        my $done = 0;
        for (my $i = 0; $i < $n; $i++) {
            $done += sprintf("%0d", ($mid/$machines->[$i]));
            last if ($done >= $goal);
        }
        if ($done >= $goal) {
            $max_days = $mid - 1;
            $result = $mid;
        } else {
            $min_days = $mid + 1;
        }
    }
    return $result;
}

open(my $fptr, '>', $ENV{'OUTPUT_PATH'});

my $nGoal = <>;
$nGoal =~ s/\s+$//;
my @nGoal = split /\s+/, $nGoal;

my $n = $nGoal[0];
$n =~ s/\s+$//;

my $goal = $nGoal[1];
$goal =~ s/\s+$//;

my $machines = <>;
$machines =~ s/\s+$//;
my @machines = split /\s+/, $machines;

my $ans = minTime \@machines, $goal, $n;

print $fptr "$ans\n";

close $fptr;
