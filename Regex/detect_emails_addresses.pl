#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

# https://www.hackerrank.com/challenges/detect-the-email-addresses/problem

my $pattern = qr/\b\S+\@\S+\.\S+\b/;

my $lines = <>;
my @emails;
foreach my $line (1..$lines) {
    my $in = <STDIN>;
    if ($in !~ /^$/) {
        (my @matches) = $in =~ /$pattern/g;
        push @emails, @matches;
    }
}

my @uniq_emails = uniq (@emails);

print  join(";", (sort {$a cmp $b} @uniq_emails));

sub uniq {
    my @emails = @_;
    my %seen;
    return grep !$seen{$_}++, @emails;
}




