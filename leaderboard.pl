#!/usr/bin/perl

use strict;
use warnings;

# Complete the climbingLeaderboard function below.
sub climbingLeaderboard {
    my $scores = shift;
    my $alice = shift;
    my @result;
    # print "@$scores\n";
    # find largest rank
    my $n = scalar(@{$scores});
    my $max_rank = 1;
    for (my $i = 1; $i < $n; $i++) {
        # print "Comparing: $scores->[$i] and $scores->[$i-1]\n";
        if ($scores->[$i] != $scores->[$i-1]) {
            $max_rank++;
        }
    }
    # print $max_rank, "\n";

    my $rank = $max_rank + 1;
    my $cursor = $n - 1;

    for (my $i = 0; $i < scalar(@$alice); $i++) {
        # print "Alice: $alice->[$i]\n";
        while ($cursor >= 0 && $alice->[$i] >= $scores->[$cursor]) {
            # print "Comparing: $alice->[$i] and $scores->[$cursor]\n";
            my $flag = 0;
            do {
                $cursor--;
                if (! $flag) {
                    $rank -= 1;
                    if ($rank <= 0) {
                        $rank = 1;
                    }
                    $flag = 1;
                }
                # print "Current rank: $rank\n";
            } while ( $cursor > 0 && $scores->[$cursor + 1] == $scores->[$cursor]);
        }
        push @result, $rank;
    }
    return @result;
}

open(my $fptr, '>', $ENV{'OUTPUT_PATH'});

my $scores_count = <>;
$scores_count =~ s/\s+$//;

my $scores = <>;
$scores =~ s/\s+$//;
my @scores = split /\s+/, $scores;

my $alice_count = <>;
$alice_count =~ s/\s+$//;

my $alice = <>;
$alice =~ s/\s+$//;
my @alice = split /\s+/, $alice;

my @result = climbingLeaderboard \@scores, \@alice;

print $fptr join "\n", @result;
print $fptr "\n";

close $fptr;
