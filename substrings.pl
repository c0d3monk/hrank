#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;

#
# Complete the countSubstrings function below.
#
# TODO: WIP
sub countSubstrings {
    my $str = shift;
    my $queries = shift;
    my @result;
    foreach my $query (@{$queries}) {
        my $len = ($query->[1] - $query->[0]) + 1;
        my $string = substr($str, $query->[0], $len);

        my $sub_len = length($string);
        my $count = find_occurence($string);
        my $x = ($sub_len * ($sub_len + 1)) / 2;
        my $final = $x - $count;
        push @result, $final;
    }
    return @result;
}

sub find_occurence {
    my $x = shift;
    my $hash = {};
    for my $i (0 .. length($x)-1) {
        my $cr = substr($x, $i, 1);
        my $c = () = $x =~ /$cr/g;
        $hash->{$cr} = $c;
    };
    my @arr;
    my $count = 0;
    foreach my $k (keys %$hash) {
        if ($hash->{$k} > 1 ) {
            $count += $hash->{$k}-1
        }
    }
    return $count;
}

open(my $fptr, '>', $ENV{'OUTPUT_PATH'});

my $nq = <>;
$nq =~ s/\s+$//;
my @nq = split /\s+/, $nq;

my $n = $nq[0];
$n =~ s/\s+$//;

my $q = $nq[1];
$q =~ s/\s+$//;

my $s = <>;
chomp($s);

my @queries = ();

for (1..$q) {
    my $queries_item = <>;
    $queries_item =~ s/\s+$//;
    my @queries_item = split /\s+/, $queries_item;

    push @queries, \@queries_item;
}

# print "n: $n; string: $s; queries: ". Dumper(@queries)."\n";

my @result = countSubstrings($s, \@queries);

print $fptr join "\n", @result;
print $fptr "\n";

close $fptr;
