#!/usr/bin/perl

# https://www.hackerrank.com/challenges/triple-sum/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=search

use strict;
use warnings;
use POSIX qw/ceil/;

# TODO: Complete the triplets function below.

sub triplets {
    my $arra = shift;
    my $arrb = shift;
    my $arrc = shift;

    my @sorted_arra = sort {$a <=> $b} @{$arra};
    my @uniq_sorted_arra = uniq(@sorted_arra);
    my @sorted_arrb = sort {$a <=> $b} @{$arrb};
    my @uniq_sorted_arrb = uniq(@sorted_arrb);
    my @sorted_arrc = sort {$a <=> $b} @{$arrc};
    my @uniq_sorted_arrc = uniq(@sorted_arrc);

    my $result = 0;

    my $x = upper_bound(sub { $_ <=> $uniq_sorted_arrb[0] }, @uniq_sorted_arra);
    my $y = upper_bound(sub { $_ <=> $uniq_sorted_arrb[0] }, @uniq_sorted_arrc);

    print "x: $x; y: $y\n";

    $result += $x*$y;

    for (my $i = 1; $i < scalar(@uniq_sorted_arrb); $i++) {
        if ($uniq_sorted_arrb[$i] != $uniq_sorted_arrb[$i-1]) {
            $x = upper_bound(sub { $_ <=> $uniq_sorted_arrb[$i] }, @uniq_sorted_arra);
            $y = upper_bound(sub { $_ <=> $uniq_sorted_arrb[$i] }, @uniq_sorted_arrc);

            $result += $x*$y;
        }
    }
    return $result;
}

sub uniq {
    my @array = @_;
    my %seen;
    grep !$seen{$_}++, @_;
}

sub upper_bound {
    my $code  = shift;
    my $count = @_;
    my $first = 0;
    while ($count > 0)
    {
        my $step = $count >> 1;
        my $it   = $first + $step;
        local *_ = \$_[$it];
        if ($code->() <= 0) {
            $first = ++$it;
            $count -= $step + 1;
        } else {
            $count = $step;
        }
    }

    return $first;
}

open(my $fptr, '>', $ENV{'OUTPUT_PATH'});

my $lenaLenbLenc = <>;
$lenaLenbLenc =~ s/\s+$//;
my @lenaLenbLenc = split /\s+/, $lenaLenbLenc;

my $lena = $lenaLenbLenc[0];
$lena =~ s/\s+$//;

my $lenb = $lenaLenbLenc[1];
$lenb =~ s/\s+$//;

my $lenc = $lenaLenbLenc[2];
$lenc =~ s/\s+$//;

my $arra = <>;
$arra =~ s/\s+$//;
my @arra = split /\s+/, $arra;

my $arrb = <>;
$arrb =~ s/\s+$//;
my @arrb = split /\s+/, $arrb;

my $arrc = <>;
$arrc =~ s/\s+$//;
my @arrc = split /\s+/, $arrc;

my $ans = triplets \@arra, \@arrb, \@arrc;

print $fptr "$ans\n";

close $fptr;
