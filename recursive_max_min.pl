#!/usr/bin/env perl

use strict;
use warnings;


my $list = [1,5,-1,55,108,99,22,9,200,7];
my $max = getMaxNum($list, 0);
my $min = getMinNum($list, 0);

print "Max: $max, Min: $min\n";

sub getMaxNum {
    my $list = shift;
    my $idx = shift;
    my $max = shift || 0;
    if ($idx == scalar(@$list)-1) {
        return $list->[$max];
    } else {
        if ($list->[$max] < $list->[$idx+1]) {
            $max = $idx+1; 
        }
        getMaxNum($list, $idx+1, $max);
    }
}

sub getMinNum {
    my $list = shift;
    my $idx = shift;
    my $min = shift || 0;
    if ($idx == scalar(@$list)-1) {
        return $list->[$min];
    } else {
        if ($list->[$min] > $list->[$idx+1]) {
            $min = $idx+1; 
        }
        getMinNum($list, $idx+1, $min);
    }
}