#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use List::Util qw/max/;

# Complete the hourglassSum function below.
sub hourglassSum {
    my $arr = shift;
    my @result;
    for (my $i = 0; $i < 4; $i++) {
        my $top = $arr->[$i];
        my $middle = $arr->[$i+1];
        my $last = $arr->[$i+2];
        for (my $j = 0; $j < 4; $j++) {
            my $sum = 0;
            foreach my $temp (0..2) {
                $sum += $top->[$j+$temp];
                $sum += $last->[$j+$temp];
            }
            $sum += $middle->[$j+1];
            push @result, $sum;
        }
    }
    my $r = max(@result);
    return $r;
}

open(my $fptr, '>', $ENV{'OUTPUT_PATH'});

my @arr = ();

for (1..6) {
    my $arr_item = <>;
    $arr_item =~ s/\s+$//;
    my @arr_item = split /\s+/, $arr_item;

    push @arr, \@arr_item;
}

my $result = hourglassSum \@arr;

print $fptr "$result\n";

close $fptr;
