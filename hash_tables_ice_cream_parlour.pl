#!/usr/bin/perl

# https://www.hackerrank.com/challenges/ctci-ice-cream-parlor/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=search

use strict;
use warnings;
use Data::Dumper;

# Complete the whatFlavors function below.
sub whatFlavors {
    my $cost = shift;
    my $money = shift;
    # my $cost_hash = create_hash_table($cost);
    my %cost_hash;
    for (my $i = 0; $i < scalar(@{$cost}); $i++) {
        my $compliment = $money - $cost->[$i];
        if (exists $cost_hash{$compliment} && $cost_hash{$compliment} != $i ) {
            my $f = $i + 1;
            my $s = $cost_hash{$compliment} + 1;
            print "$s $f\n";
            delete @cost_hash{keys %cost_hash};
            return;
        }
        $cost_hash{$cost->[$i]} = $i;
    }
}

sub create_hash_table {
    my $cost = shift;
    my $i = 0;
    my %h_table;
    foreach my $c (@{$cost}) {
        $h_table{$c} = $i;
        $i++;
    }
    return \%h_table;
}

my $t = <>;
$t =~ s/\s+$//;

for (my $t_itr = 0; $t_itr < $t; $t_itr++) {
    my $money = <>;
    $money =~ s/\s+$//;

    my $n = <>;
    $n =~ s/\s+$//;

    my $cost = <>;
    $cost =~ s/\s+$//;
    my @cost = split /\s+/, $cost;

    whatFlavors(\@cost, $money);
}
