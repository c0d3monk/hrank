#!/usr/bin/perl

use strict;
use warnings;

# Complete the rotLeft function below.
sub rotLeft {
    my $array = shift;
    my @new_array = @$array;
    my $rotations = shift;
    while ($rotations) {
        push(@new_array, (shift(@new_array)));
        $rotations--;
    }
    return @new_array;
}

open(my $fptr, '>', $ENV{'OUTPUT_PATH'});

my $nd = <>;
$nd =~ s/\s+$//;
my @nd = split /\s+/, $nd;

my $n = $nd[0];
$n =~ s/\s+$//;

my $d = $nd[1];
$d =~ s/\s+$//;

my $a = <>;
$a =~ s/\s+$//;
my @a = split /\s+/, $a;

my @result = rotLeft \@a, $d;

print $fptr join " ", @result;
print $fptr "\n";

close $fptr;
