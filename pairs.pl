#!/usr/bin/perl

# https://www.hackerrank.com/challenges/pairs?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=search

use strict;
use warnings;

# Complete the pairs function below.
sub pairs {
    my $target = shift;
    my $array = shift;
    my %temp_hash = create_hash_table($array);
    my $count = 0;
    for (my $i = 0; $i < scalar(@{$array}); $i++) {
        my $compliment = $target + $array->[$i];
        if (exists $temp_hash{$compliment} && $temp_hash{$compliment} != $i) {
            $count++;
        }
    }
    delete(@temp_hash{keys %temp_hash});
    return $count;
}

sub create_hash_table {
    my $array = shift;
    my %hash;
    for (my $i = 0; $i < scalar(@{$array}); $i++) {
        $hash{$array->[$i]} = $i;
    }
    return %hash;
}

open(my $fptr, '>', $ENV{'OUTPUT_PATH'});

my $nk = <>;
$nk =~ s/\s+$//;
my @nk = split /\s+/, $nk;

my $n = $nk[0];
$n =~ s/\s+$//;

my $k = $nk[1];
$k =~ s/\s+$//;

my $arr = <>;
$arr =~ s/\s+$//;
my @arr = split /\s+/, $arr;

my $result = pairs $k, \@arr;

print $fptr "$result\n";

close $fptr;
